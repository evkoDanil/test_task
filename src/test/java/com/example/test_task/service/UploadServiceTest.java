package com.example.test_task.service;

import com.example.test_task.dto.FindParams;
import com.example.test_task.model.UploadInfo;
import com.example.test_task.parser.interf.FileParser;
import com.example.test_task.repository.UploadInfoRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Date;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UploadServiceTest {

    @Mock
    private FileParser fileParser;
    @Mock
    private UploadInfoRepository uploadInfoRepository;
    @Mock
    private CarInfoService carInfoService;
    @InjectMocks
    private UploadService uploadService;

    UploadInfo uploadInfo = UploadInfo.builder()
            .date(new Date(150L))
            .filename("upload fileName")
            .id(123L)
            .build();


    @Test
    public void findByDate() {
        Date from = new Date(100L);
        Date to = new Date(250L);
        FindParams findParams = new FindParams(from, to);
        when(uploadInfoRepository.getUploadInfosByDateBetween(from, to)).thenReturn(Collections.singletonList(uploadInfo));

        uploadService.findByDate(findParams);

        verify(uploadInfoRepository, times(1)).getUploadInfosByDateBetween(from, to);
    }

}
