package com.example.test_task.service;

import com.example.test_task.exception.NullIdException;
import com.example.test_task.model.CarInfo;
import com.example.test_task.repository.CarInfoRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CarInfoTest {

    @Mock
    private CarInfoRepository carInfoRepository;

    @InjectMocks
    private CarInfoService carInfoService;

    @Test
    public void getCarInfoByUploadId() {
        Long id = 10L;
        when(carInfoRepository.findAllByUploadInfo_Id(id)).thenReturn(Collections.singletonList(CarInfo.builder().build()));

        try {
            carInfoService.getCarInfoByUploadId(id);
        } catch (NullIdException e) {
            System.out.println(e.getMessage());
            fail();
        }

        verify(carInfoRepository, times(1)).findAllByUploadInfo_Id(id);
    }

    @Test
    public void getCarInfoByUploadId_when_id_is_null() {
        Long id = null;

        try {
            carInfoService.getCarInfoByUploadId(id);
        } catch (NullIdException e) {
            System.out.println(e.getMessage());
        }

        verify(carInfoRepository, times(0)).findAllByUploadInfo_Id(id);
    }
}
