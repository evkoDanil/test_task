package com.example.test_task.parser.impl;

import com.example.test_task.enums.Marks;
import com.example.test_task.model.CarInfo;
import com.example.test_task.parser.interf.FileParser;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service
@Slf4j
@Data
@AllArgsConstructor
public class FileParserImpl implements FileParser {

    @Override
    public List<CarInfo> parseExcel(MultipartFile multipartFile) throws IOException {
        InputStream excelIs = multipartFile.getInputStream();
        Workbook workbook = WorkbookFactory.create(excelIs);
        List<CarInfo> carInfos = new ArrayList<>();

        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            Sheet sheet = workbook.getSheetAt(i);
            Iterator<Row> rowIterator = sheet.rowIterator();

            while (rowIterator.hasNext()) {
                Row currentRow = rowIterator.next();
                //Пропускаем первый ряд
                if (!currentRow.getCell(0).getStringCellValue().equals("Наименование")) {

                    Iterator<Cell> cellIterator = currentRow.cellIterator();
                    CarInfo carInfo = new CarInfo();
                    boolean isRequired = false;
                    while (cellIterator.hasNext()) {
                        Cell currentCell = cellIterator.next();
                        //Пропускать значения вне справочника
                        if (currentCell.getCellType() == CellType.STRING &&
                                Marks.BMW.getCars().stream().anyMatch(str -> str.equals(currentCell.getStringCellValue()))) {
                            carInfo.setName(currentCell.getStringCellValue());
                            isRequired = true;
                        }
                        if (currentCell.getCellType() == CellType.NUMERIC && isRequired) {
                            if (DateUtil.isCellDateFormatted(currentCell)) {
                                carInfo.setDate(DateUtil.getJavaDate(currentCell.getNumericCellValue()));
                            } else {
                                carInfo.setId(Long.toString((long) currentCell.getNumericCellValue()));
                            }
                        }
                    }
                    if (carInfo.getId() != null || carInfo.getDate() != null || carInfo.getName() != null) {
                        carInfos.add(carInfo);
                    }
                }
            }
        }
        log.info("parsed xlsx file {}", multipartFile.getOriginalFilename());
        return carInfos;
    }

    @Override
    public List<CarInfo> parseCSV(MultipartFile multipartFile) throws IOException {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(multipartFile.getInputStream()));
            CSVReader csvReader = new CSVReader(reader);
            List<CarInfo> carInfos = new ArrayList<>();
            String[] str = csvReader.readNext();
            while (str != null) {
                str = csvReader.readNext();
                if (str != null) {
                    String[] values = str[0].split(";");
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String name = values[0];
                    Date date = formatter.parse(values[1]);
                    String id = values[2];

                    if (Marks.BMW.getCars().stream().anyMatch(s -> s.equals(name))) {
                        CarInfo carInfo = CarInfo.builder()
                                .name(name)
                                .id(id)
                                .date(date)
                                .build();
                        if (carInfo.getId() != null || carInfo.getDate() != null || carInfo.getName() != null) {
                            carInfos.add(carInfo);
                        }
                    }
                }
            }
            return carInfos;
        } catch (IOException | CsvValidationException |
                ParseException e) {
            log.error(e.getMessage());
            return null;
        }
    }
}
