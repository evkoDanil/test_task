package com.example.test_task.parser.interf;

import com.example.test_task.model.CarInfo;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface FileParser {
    List<CarInfo> parseExcel(MultipartFile multipartFile) throws IOException;
    List<CarInfo> parseCSV(MultipartFile multipartFile) throws IOException;
}
