package com.example.test_task.enums;

import java.util.ArrayList;
import java.util.List;

public enum Marks {
    Toyota,
    Kia,
    Honda,
    BMW;

    public List<String> getCars() {
        return new ArrayList<>() {{
            add(Marks.Toyota.name());
            add(Marks.Kia.name());
            add(Marks.Honda.name());
            add(Marks.BMW.name());
        }};
    }
}
