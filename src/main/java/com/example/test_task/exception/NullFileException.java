package com.example.test_task.exception;

public class NullFileException extends Exception {
    public NullFileException(String message) {
        super(message);
    }
}
