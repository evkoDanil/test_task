package com.example.test_task.exception;

public class NullFilenameException extends Exception {
    public NullFilenameException(String message) {
        super(message);
    }
}
