package com.example.test_task.exception;

public class NullIdException extends Exception{
    public NullIdException(String message){
        super(message);
    }
}
