package com.example.test_task.сontroller;

import com.example.test_task.exception.NullIdException;
import com.example.test_task.model.CarInfo;
import com.example.test_task.service.CarInfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/car_info")
@RequiredArgsConstructor
@Slf4j
public class CarInfoController {
    private final CarInfoService carInfoService;

    @GetMapping("/upload/{uploadId}")
    public List<CarInfo> getCarInfoByUploadId(@PathVariable Long uploadId) {
        try {
            return carInfoService.getCarInfoByUploadId(uploadId);
        } catch (NullIdException e) {
            log.error(e.getMessage());
            return null;
        }
    }
}
