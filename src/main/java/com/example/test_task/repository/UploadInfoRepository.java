package com.example.test_task.repository;

import com.example.test_task.model.UploadInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface UploadInfoRepository extends JpaRepository<UploadInfo,String> {
    List<UploadInfo> getUploadInfosByDateBetween(Date from, Date to);
}
