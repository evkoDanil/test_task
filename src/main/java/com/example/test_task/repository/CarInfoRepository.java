package com.example.test_task.repository;

import com.example.test_task.model.CarInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarInfoRepository extends JpaRepository<CarInfo, String> {
    List<CarInfo> findAllByUploadInfo_Id(Long uploadId);
}
