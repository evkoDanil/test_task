package com.example.test_task.service;

import com.example.test_task.dto.FindParams;
import com.example.test_task.exception.NullFileException;
import com.example.test_task.exception.NullFilenameException;
import com.example.test_task.model.CarInfo;
import com.example.test_task.model.UploadInfo;
import com.example.test_task.parser.interf.FileParser;
import com.example.test_task.repository.UploadInfoRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class UploadService {
    private final FileParser fileParser;
    private final UploadInfoRepository uploadInfoRepository;
    private final CarInfoService carInfoService;

    public Long uploadFile(MultipartFile multipartFile) throws NullFilenameException, NullFileException {
        if(multipartFile != null) {
            if (multipartFile.getOriginalFilename() != null) {
                String fileName = multipartFile.getOriginalFilename();
                UploadInfo upload = createNewUpload(fileName);
                List<CarInfo> carInfos = new ArrayList<>();
                try {
                    if (fileName.endsWith(".xlsx")) {
                        carInfos = fileParser.parseExcel(multipartFile);

                    } else if (fileName.endsWith(".csv")) {
                        carInfos = fileParser.parseCSV(multipartFile);
                    }
                    if (!carInfos.isEmpty()) {
                        carInfoService.saveMany(carInfos, upload);
                        return upload.getId();
                    }
                } catch (IOException e) {
                    log.error(e.getMessage());
                    return null;
                }
            }
            throw new NullFilenameException("file name is null");
        }
        else throw new NullFileException("file is null");
    }

    public List<UploadInfo> findByDate(FindParams findParams) {
        if (findParams.getFrom() != null) {
            if (findParams.getTo() == null) {
                return uploadInfoRepository.getUploadInfosByDateBetween(
                        findParams.getFrom(),
                        new Date());
            } else if (findParams.getFrom().getTime() < findParams.getTo().getTime()) {
                return uploadInfoRepository.getUploadInfosByDateBetween(
                        findParams.getFrom(),
                        findParams.getTo()
                );
            }
        } else {
            Date from = new Date();
            if (from.getTime() < findParams.getTo().getTime()) {
                return uploadInfoRepository.getUploadInfosByDateBetween(
                        from,
                        findParams.getTo()
                );
            }
        }
        return null;
    }

    private UploadInfo createNewUpload(String fileName) {
        UploadInfo upload = UploadInfo.builder()
                .date(new Date())
                .filename(fileName)
                .build();
        uploadInfoRepository.save(upload);
        return upload;
    }
}
