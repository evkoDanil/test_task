package com.example.test_task.service;

import com.example.test_task.exception.NullIdException;
import com.example.test_task.model.CarInfo;
import com.example.test_task.model.UploadInfo;
import com.example.test_task.repository.CarInfoRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class CarInfoService {
    private final CarInfoRepository carInfoRepository;

    public List<CarInfo> getCarInfoByUploadId(Long uploadId) throws NullIdException {
        if (uploadId == null) {
            throw new NullIdException("upload id is null");
        }
        return carInfoRepository.findAllByUploadInfo_Id(uploadId);
    }

    public void save(CarInfo carInfo) {
        carInfoRepository.save(carInfo);
    }

    public void saveMany(List<CarInfo> carInfos, UploadInfo upload) {
        carInfoRepository.saveAll(carInfos);
        log.info("saved cars with upload id {}", upload.getId());
    }
}
